package test.data_structures;

import model.data_structures.DoublyLinkedList;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class TestDoublyLinkedList {

	private DoublyLinkedList<Integer> listTest;
	private static int EXPECTED_SIZE=100;

	@Before
	public void setUp1() {
		listTest = new DoublyLinkedList<Integer>();
	}

	public void setUp2() {
		for(int i =0; i< EXPECTED_SIZE*2; i++){
			listTest.append(i);
		}
	}

	public void setUp3() {
		listTest = new DoublyLinkedList<Integer>(1);
	}

	@Test
	public void testDoublyLinkedList() {
		assertTrue(listTest!=null);
		assertEquals(0, listTest.size());
		assertTrue(listTest.isEmpty());
		setUp3();
		assertEquals(1, listTest.size());
		assertEquals(1, (int)listTest.get(0));
	}

	@Test
	public void testGet() {
		setUp2();
		assertEquals(0, (int)listTest.get(0));
		assertEquals(EXPECTED_SIZE, (int)listTest.get(EXPECTED_SIZE));
		assertEquals((EXPECTED_SIZE*2-1), (int)listTest.get(EXPECTED_SIZE*2-1));
		try
		{
			listTest.get(EXPECTED_SIZE*3);
			fail();
		}
		catch(IndexOutOfBoundsException e){
		}
	}

	@Test
	public void testAddFirst() {
		listTest.addFirst(123);
		assertEquals(123, (int)listTest.get(0));
		assertEquals(1, listTest.size());
		setUp2();
		assertEquals(EXPECTED_SIZE*2+1, listTest.size());
		assertEquals(0, (int)listTest.get(1));
		assertEquals(EXPECTED_SIZE, (int)listTest.get(EXPECTED_SIZE+1));
		assertEquals((EXPECTED_SIZE*2-1), (int)listTest.get(EXPECTED_SIZE*2));
		assertEquals(123, (int)listTest.get(0));
		listTest.addFirst(333);
		assertEquals(333, (int)listTest.get(0));
	}

	@Test
	public void testAppend() {
		listTest.append(123);
		assertEquals(123, (int)listTest.get(0));
		setUp2();
		assertEquals(0, (int)listTest.get(1));
		assertEquals(EXPECTED_SIZE, (int)listTest.get(EXPECTED_SIZE+1));
		assertEquals((EXPECTED_SIZE*2-1), (int)listTest.get(EXPECTED_SIZE*2));
		assertEquals(123, (int)listTest.get(0));
		listTest.append(333);
		assertEquals(333, (int)listTest.get(EXPECTED_SIZE*2+1));
		assertEquals(EXPECTED_SIZE*2+2, listTest.size());
	}

	@Test
	public void testRemoveFirst() {
		try
		{
			listTest.removeFirst();
			fail();
		}
		catch(IndexOutOfBoundsException e){
		}
		listTest.append(123);
		listTest.removeFirst();
		assertEquals(0, listTest.size());
		listTest.append(123);
		listTest.append(123);
		listTest.removeFirst();
		assertEquals(1, listTest.size());
		listTest.removeFirst();
		setUp2();
		listTest.removeFirst();
		assertEquals(1, (int)listTest.get(0));
		assertEquals(EXPECTED_SIZE+1, (int)listTest.get(EXPECTED_SIZE));
		assertEquals(EXPECTED_SIZE*2-1, listTest.size());	
	}

	@Test
	public void testRemove() {
		try
		{
			listTest.remove(1);
			fail();
		}
		catch(IndexOutOfBoundsException e){
		}
		listTest.append(123);
		listTest.remove(0);
		assertEquals(0, listTest.size());
		listTest.append(123);
		listTest.append(333);
		listTest.remove(1);
		assertEquals(123, (int)listTest.get(0));
		assertEquals(1, listTest.size());	
		listTest.remove(0);
		setUp2();
		listTest.remove(EXPECTED_SIZE);
		assertEquals(0, (int)listTest.get(0));
		assertEquals(EXPECTED_SIZE-1, (int)listTest.get(EXPECTED_SIZE-1));
		assertEquals(EXPECTED_SIZE+1, (int)listTest.get(EXPECTED_SIZE));
		assertEquals(EXPECTED_SIZE*2-1, listTest.size());	
	}

	@Test
	public void testSize() {
		assertEquals(0, listTest.size());
		setUp2();
		assertEquals(EXPECTED_SIZE*2, listTest.size());
	}

	@Test
	public void testIsEmpty() {
		assertTrue(listTest.isEmpty());
		setUp2();
		assertFalse(listTest.isEmpty());
	}
	
	@Test
	public void testIterator() {
		Iterator<Integer> it = listTest.iterator();
		assertFalse(it.hasNext());
		setUp2();
		assertTrue(it.hasNext());
		assertEquals(listTest.get(0), it.next());
	}
}
