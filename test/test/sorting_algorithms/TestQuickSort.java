package test.sorting_algorithms;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoublyLinkedList;
import model.sorting_algorithms.QuickSort;

public class TestQuickSort {

	private DoublyLinkedList<Integer> listTest;

	private QuickSort<Integer> test;

	private static int EXPECTED_SIZE=100;

	@Before
	public void setUp1() {
		listTest = new DoublyLinkedList<Integer>();
		test = new QuickSort<Integer>(listTest);
	}

	public void setUp2() {
		for(int i = 0; i< EXPECTED_SIZE*2; i++)
		{
			listTest.append((EXPECTED_SIZE*2)-i);
		}
		test = new QuickSort<Integer>(listTest);
	}

	@Test
	public void testQuickSort() {
		assertNotNull(test.getArray());
	}

	@Test
	public void testSort() {
		setUp2();
		test.sort();
		Comparable<Integer>[] array = test.getArray();
		Integer past = -1;
		for(Comparable<Integer> num : array)
		{
			System.out.println((int)num);
			assertTrue(num.compareTo(past) >= 0);
			past = (Integer) num;
		}
	}
}
