package test.sorting_algorithms;


import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoublyLinkedList;
import model.sorting_algorithms.MergeSort;

public class TestMergeSort {

	private DoublyLinkedList<Integer> listTest;

	private MergeSort<Integer> test;

	private static int EXPECTED_SIZE=100;

	@Before
	public void setUp1() {
		listTest = new DoublyLinkedList<Integer>();
		test = new MergeSort<Integer>(listTest);
	}

	public void setUp2() {
		for(int i = 0; i< EXPECTED_SIZE*2; i++)
		{
			listTest.append((EXPECTED_SIZE*2)-i);
		}
		test = new MergeSort<Integer>(listTest);
	}

	@Test
	public void testMergeSort() {
		assertNotNull(test.getArray());
	}

	@Test
	public void testSort() {
		setUp2();
		test.sort();
		Comparable<Integer>[] array = test.getArray();
		Integer past = -1;
		for(Comparable<Integer> num : array)
		{
			System.out.println((int)num);
//			assertTrue(num.compareTo(past) >= 0);
			past = (Integer) num;
		}
	}
}
