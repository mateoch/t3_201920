package test.sorting_algorithms;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoublyLinkedList;
import model.sorting_algorithms.MergeSort;
import model.sorting_algorithms.SortingAlgorithm;

public class TestSortingAlgorithm {


	private DoublyLinkedList<Integer> listTest;
	
	private SortingAlgorithm<Integer> test;
	
	private static int EXPECTED_SIZE=100;

	@Before
	public void setUp1() {
		listTest = new DoublyLinkedList<Integer>();
		test = new SortingAlgorithm<Integer>(listTest);
	}

	public void setUp2() {
		for(int i = 0; i< EXPECTED_SIZE*2; i++)
		{
			listTest.append((EXPECTED_SIZE*2)-i);
		}
		test = new SortingAlgorithm<Integer>(listTest);
	}

	@Test
	public void testSortingAlgorithm() {
		assertNotNull(test.getArray());
	}
	
	@Test
	public void testGetArray() {
		setUp2();
		Comparable<Integer>[] array = test.getArray();
		assertEquals(EXPECTED_SIZE*2, array[0]);
		assertEquals(1, array[(EXPECTED_SIZE*2)-1]);
	}

	@Test
	public void testLess() {
		assertTrue(test.less(new Integer(1), new Integer(10)));
		assertTrue(test.less(new Integer(10), new Integer(10)));
		assertFalse(test.less(new Integer(10), new Integer(1)));
	}

	@Test
	public void testGreater() {
		assertTrue(test.greater(new Integer(10), new Integer(1)));
		assertFalse(test.greater(new Integer(10), new Integer(10)));
		assertFalse(test.greater(new Integer(1), new Integer(10)));
	}
	
	@Test
	public void testExch() {
		setUp2();
		test.exch(test.getArray(), 0, (EXPECTED_SIZE*2)-1);
		assertEquals(1, test.getArray()[0]);
		assertEquals(EXPECTED_SIZE*2, test.getArray()[(EXPECTED_SIZE*2)-1]);
	}
	
	
}
