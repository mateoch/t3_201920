package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar viajes");
			System.out.println("2. Consultar viajes de una hora");
			System.out.println("3. Ordenar los viajes consultados usando ShellSort");
			System.out.println("4. Ordenar los viajes consultados usando MergeSort");
			System.out.println("5. Ordenar los viajes consultados usando QuickSort");
			System.out.println("6. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
