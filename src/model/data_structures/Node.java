package model.data_structures;

public class Node<T> {

	private Node<T> next;

	private Node<T> previous;

	private T item;

	public Node (T item) 
	{
		next = null;
		previous = null;
		this.item = item;
	}
	
	public Node<T> getNext() 
	{
		return next;
	}
	
	public Node<T> getPrevious() 
	{
		return previous;
	}
	
	public void setNext( Node<T> newNext) 
	{
		this.next = newNext;
	}
	
	public void setPrevious( Node<T> newPrevious) 
	{
		this.previous = newPrevious;
	}
	
	public T getItem()
	{
		return item;
	}
	
	public void setItem (T item) 
	{
		this.item = item;
	}

}
