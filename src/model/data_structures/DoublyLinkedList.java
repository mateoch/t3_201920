package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * 2019-01-23
 * Lista doblemente encadenada genérica.
 * @author Nicolas Urrego Sandoval
 *
 */
public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {

	private class DoublyLinkedListIterator implements Iterator<T>
	{	
		private Node<T> currentNode = null;
		@Override
		public boolean hasNext()
		{
			if(currentNode==null)
				currentNode = head;
			else
				currentNode = currentNode.getNext();
			return currentNode!=null? true : false;
		}

		@Override
		public T next()
		{
			if(currentNode!=null)
				return currentNode.getItem();
			else
				throw new NoSuchElementException();
		}		
	}

	/**
	 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
	 */
	private int size;

	/**
	 * Cabeza de la lista
	 */
	private Node<T> head;

	/**
	 * Cola de la lista
	 */
	private Node<T> tail;


	public DoublyLinkedList()
	{
		head = null;
		tail = null;
		size = 0;
	}

	public DoublyLinkedList(T item)
	{
		head = new Node<T>(item);
		tail = null;
		size = 1;
	}

	public void addFirst(T item)
	{
		Node <T> newHead = new Node<>(item);
		if(head==null)
		{
			head = newHead;
		}
		else if(tail==null)
		{
			newHead.setNext(head);
			head.setPrevious(newHead);
			tail = head;
			head = newHead;
		}
		else
		{
			newHead.setNext(head);
			head.setPrevious(newHead);
			head = newHead;
		}
		size++;		
	}

	public void append(T item)
	{
		Node <T> newNode = new Node<T>(item);
		if(head==null)
		{
			head = newNode;
		}
		else if(tail==null)
		{
			newNode.setPrevious(head);
			head.setNext(newNode);
			tail = newNode;
		}
		else
		{
			newNode.setPrevious(tail);
			tail.setNext(newNode);
			tail = newNode;
		}
		size++;
	}

	public void removeFirst()
	{
		if(head!=null)
		{
			if(head.getNext()!=null)
			{
				head.getNext().setPrevious(null);
			}
			Node<T> newHead = head.getNext();
			head.setNext(null);
			head = newHead;
			size--;
			if(size == 1)
			{
				tail = null;
			}
		}
		else
			throw new IndexOutOfBoundsException();
	}

	public void remove (int pos)
	{
		if(pos>0 && pos<size-1)
		{
			boolean stop = false;
			Node<T> current = head.getNext();
			for(int i=1; i<size-1 && !stop; i++)
			{

				if(i==pos)
				{
					current.getPrevious().setNext(current.getNext());
					current.getNext().setPrevious(current.getPrevious());
					current.setNext(null);
					current.setPrevious(null);
					stop = true;
				}
				else
				{
					current = current.getNext();
				}
			}
			size--;
		}
		else if(pos==0)
		{
			removeFirst();
		}
		else if(pos==size-1)
		{
			tail.getPrevious().setNext(null);
			Node<T> newTail = tail.getPrevious();
			tail.setPrevious(null);
			tail = newTail;
			size--;
			if(size == 1)
			{
				tail = null;
			}
		}
		else
			throw new IndexOutOfBoundsException();
	}

	public T get (int pos)
	{
		Node<T> search = null;
		if(pos>=0 && pos<size)
		{
			if(head!=null)
			{
				boolean stop = false;
				Node<T> current = head;
				for(int i=0; i<size && !stop; i++)
				{

					if(i==pos)
					{
						search = current;
						stop = true;
					}
					else
					{
						current = current.getNext();
					}
				}
			}
		}
		else
			throw new IndexOutOfBoundsException();

		return search!=null? search.getItem() : null;
	}

	public int size()
	{
		return size;
	}

	public boolean isEmpty()
	{
		return head==null? true : false;
	}

	@Override
	public Iterator<T> iterator() {
		return new DoublyLinkedListIterator();
	}

}
