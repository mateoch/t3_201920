package model.logic;

public class Trip implements Comparable<Trip>{
	
	private int sourceid, dstid, timeParameter;
	private double mean_travel_time, standard_deviation_travel_time, geometric_mean_travel_time, geometric_standard_deviation_travel_time;
	
	public Trip(String[] info)
	{
		sourceid = Integer.parseInt(info[0]);
		dstid = Integer.parseInt(info[1]);
		timeParameter = Integer.parseInt(info[2]);
		mean_travel_time = Double.parseDouble(info[3]);
		standard_deviation_travel_time = Double.parseDouble(info[4]);
		geometric_mean_travel_time = Double.parseDouble(info[5]);
		geometric_standard_deviation_travel_time = Double.parseDouble(info[6]);
	}
	
	public int getSourceid()
	{
		return sourceid;
	}
	
	public int getDstid()
	{
		return dstid;
	}
	
	public int getTimeParameter()
	{
		return timeParameter;
	}
	
	public double getMean_travel_time()
	{
		return mean_travel_time;
	}
	
	public double getStandard_deviation_travel_time()
	{
		return standard_deviation_travel_time;
	}
	
	public double getGeometric_mean_travel_time()
	{
		return geometric_mean_travel_time;
	}
	
	public double getGeometric_standard_deviation_travel_time()
	{
		return geometric_standard_deviation_travel_time;
	}

	public int compareTo(Trip that)
	{
		if(this.getMean_travel_time() > that.getMean_travel_time())
			return 1;
		else if(this.getMean_travel_time() < that.getMean_travel_time())
			return -1;
		else
		{
			if(this.getStandard_deviation_travel_time() > that.getStandard_deviation_travel_time())
				return 1;
			else if(this.getStandard_deviation_travel_time() < that.getStandard_deviation_travel_time())
				return -1;
			else
			{
				return 0;
			}
		}
		
	}
}
