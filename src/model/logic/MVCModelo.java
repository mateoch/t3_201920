package model.logic;

import java.io.FileReader;

import com.opencsv.CSVReader;

import model.data_structures.DoublyLinkedList;
import model.sorting_algorithms.MergeSort;
import model.sorting_algorithms.QuickSort;
import model.sorting_algorithms.ShellSort;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private DoublyLinkedList<Trip> dataHour; //ORDER BY: HOUR
	
	private int actHour;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		dataHour = new DoublyLinkedList<Trip>();
	}


	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public DoublyLinkedList<Trip> trips()
	{
		return dataHour;
	}
	
	public int getActHour()
	{
		return actHour;
	}
	
	public int sizeHourList()
	{
		return dataHour.size();
	}
	
	public Trip getFirstElement()
	{
		return dataHour.get(0);
	}
	
	public Trip getLastElement()
	{
		return dataHour.get(dataHour.size()-1);
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void load() throws Exception
	{
		CSVReader reader = null;
		try {
			String route = "data/bogota-cadastral-2018-2-All-HourlyAggregate.csv";
			reader = new CSVReader(new FileReader(route));
			String [] nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null)
			{
				dataHour.append(new Trip(nextLine));
			}
		} finally {
			reader.close();
		}
	}
	
	public DoublyLinkedList<Trip> tripsByHour(int pHour)
	{
		actHour=pHour;
		DoublyLinkedList<Trip> tripsWanted = new DoublyLinkedList<Trip>();
		for(Trip act: dataHour)
		{
			if(act.getTimeParameter()==pHour)
			{
				tripsWanted.append(act);
			}
		}
		return tripsWanted;
	}
	
	public Comparable<Trip>[] shell(DoublyLinkedList<Trip> pList)
	{
		ShellSort<Trip> toSort = new ShellSort<Trip>(pList);
		toSort.sort();
		return toSort.getArray();
		
	}
	
	public Comparable<Trip>[] merge(DoublyLinkedList<Trip> pList)
	{
		MergeSort<Trip> toSort = new MergeSort<Trip>(pList);
		toSort.sort();
		return toSort.getArray();
		
	}
	
	public Comparable<Trip>[] quick(DoublyLinkedList<Trip> pList)
	{
		QuickSort<Trip> toSort = new QuickSort<Trip>(pList);
		toSort.sort();
		return toSort.getArray();
		
	}
	
	
}
