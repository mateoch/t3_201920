package model.sorting_algorithms;

import model.data_structures.DoublyLinkedList;

public class MergeSort<T extends Comparable<T>> extends SortingAlgorithm<T>
{
	public MergeSort(DoublyLinkedList<T> dataStructure)
	{
		super(dataStructure);
	}

	private Comparable<T>[] aux;
	// auxiliary array for merges
	@SuppressWarnings("unchecked")
	public void sort() {
		aux = toBeSorted; //(Comparable<T>[]) new Comparable[toBeSorted.length];
		sort(toBeSorted, aux, 0, toBeSorted.length - 1); // Allocate space just once.
	}
	
	private void sort(Comparable<T>[] a, Comparable<T>[] aux, int lo, int hi)
	{
		if (hi <= lo)
			return;
		int mid = lo + (hi - lo) / 2;
		sort (aux, a, lo, mid);
		sort (aux, a, mid+1, hi);
		if (!less(a[mid+1], a[mid]))
			return;
		merge(a, aux, lo, mid, hi);
	} 

	private void merge(Comparable<T>[] a, Comparable<T>[] aux, int lo, int mid, int hi)
	{
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if (i > mid)
				aux[k] = a[j++];
			else if (j > hi)
				aux[k] = a[i++];
			else if (less(a[j], a[i]))
				aux[k] = a[j++];
			else
				aux[k] = a[i++];
		}
	}

}
