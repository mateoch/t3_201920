package model.sorting_algorithms;

import model.data_structures.DoublyLinkedList;

public class SortingAlgorithm<T extends Comparable<T>> {

	protected Comparable<T>[] toBeSorted;

	public SortingAlgorithm(DoublyLinkedList<T> dataStructure)
	{
		toBeSorted = decouple(dataStructure);
	}
	
//	public SortingAlgorithm(Iterable<T> dataStructure)
//	{
//		toBeSorted = decouple(dataStructure);
//	}
	
	public Comparable<T>[] getArray()
	{
		return toBeSorted;
	}
	
	@SuppressWarnings("unchecked")
	public boolean less(Comparable<T> a, Comparable<T> b)
	{
		boolean answer = false;
		if(a.compareTo((T) b) < 0)
		{
			answer=true;
		}
		return answer;
	}
	
	@SuppressWarnings("unchecked")
	public boolean greater(Comparable<T> a, Comparable<T> b)
	{
		boolean answer = false;
		if(a.compareTo((T) b) >= 0)
		{
			answer=true;
		}
		return answer;
	}
	
	public void exch(Comparable<T>[] pArray, int pPos1, int pPos2  )
	{
		Comparable<T> pos1=pArray[pPos1];
		pArray[pPos1]=pArray[pPos2];
		pArray[pPos2]=pos1;
	}
	
	@SuppressWarnings("unchecked")
	private Comparable<T>[] decouple(DoublyLinkedList<T> dataStructure)
	{
		Comparable<T>[] array = (Comparable<T>[]) new Comparable[dataStructure.size()];
		for(int i = 0; i < dataStructure.size(); i++)
		{
			array[i] = dataStructure.get(i);
		}
		return array;
	}
	
//	private Comparable<T>[] decouple(Iterable<T> dataStructure)
//	{
//		int size = 0;
//		for(T item : dataStructure)
//		{
//			size++;
//		}
//		Comparable<T>[] array = new (Comparable<T>[])Comparable[size];
//		int pos = 0;
//		for(T item : dataStructure)
//		{
//			array[pos] = item;
//		}
//		return array;
//	}
}
