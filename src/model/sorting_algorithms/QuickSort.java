package model.sorting_algorithms;

import java.util.Random;

import model.data_structures.DoublyLinkedList;

public class QuickSort<T extends Comparable<T>> extends SortingAlgorithm<T>
{
	public QuickSort(DoublyLinkedList<T> dataStructure)
	{
		super(dataStructure);
	}

	public void sort()
	{
		shuffle(toBeSorted); // Eliminate dependence on input.
		sort(toBeSorted, 0, toBeSorted.length - 1);
	}

//	private static void sort(Comparable[] a, int lo, int hi)
//	{
//		if (hi <= lo + CUTOFF - 1)
//		{
//			Insertion.sort(a, lo, hi);
//			return;
//		}
//		int j = partition(a, lo, hi);
//		sort(a, lo, j-1);
//		sort(a, j+1, hi);
//	}

	private void sort(Comparable<T>[] a, int lo, int hi)
	{
		if (hi <= lo) return;
		int j = partition(a, lo, hi);
		sort(a, lo, j-1); // Sort left part a[lo .. j-1].
		sort(a, j+1, hi); // Sort right part a[j+1 .. hi].
	}

	private int partition(Comparable<T>[] a, int lo, int hi)
	{ // Partition into a[lo..i-1], a[i], a[i+1..hi].
		int i = lo, j = hi+1; // left and right scan indices
		Comparable<T> v = a[lo]; // partitioning item
		while (true)
		{ // Scan right, scan left, check for scan complete, and exchange.
			while (less(a[++i], v))
				if (i == hi)
					break;
			while (less(v, a[--j]))
				if (j == lo)
					break;
			if (i >= j)
				break;
			exch(a, i, j);
		}
		exch(a, lo, j); // Put v = a[j] into position
		return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
	}

    /**
     * Fisher–Yates shuffle.
     */
    @SuppressWarnings("unchecked")
	private void shuffle(Comparable<T>[] a2)
    {
        Random rnd = new Random();
    	for (int i = a2.length - 1; i > 0; i--)
    	{
            int index = rnd.nextInt(i + 1);
            T a = (T) a2[index];
            a2[index] = a2[i];
            a2[i] = a;
        }
    }
}
