package model.sorting_algorithms;

import model.data_structures.DoublyLinkedList;

public class ShellSort<T extends Comparable<T>> extends SortingAlgorithm<T>{
	
	public ShellSort(DoublyLinkedList<T> dataStructure)
	{
		super(dataStructure);
	}

	public void sort()
	{
		int n = toBeSorted.length;
		int h = 1;
		while(h < n/3)
		{
			h = 3*h+1;
		}
		while(h>=1)
		{
			for(int i=h; i<n; i++)
			{
				for(int j=i; j>=h && less(toBeSorted[j],toBeSorted[j-h]);j-=h)
				{
					exch(toBeSorted, j, j-h);
				}
			}
			h = h/3;
		}
	}

}
