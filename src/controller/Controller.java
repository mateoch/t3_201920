package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import model.logic.Trip;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					view.printMessage("--------- \nCargando viajes: ");
				    modelo = new MVCModelo(); 
				try {
					modelo.load();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				    view.printMessage("Viajes cargados");
				    view.printMessage("N�mero de viajes a con la hora deseada: " + modelo.trips().size() + "\n---------");
				    view.printMessage("Primer viaje:  \nzona origen: "+ modelo.trips().get(0).getSourceid()+"\nzona destino: "+ modelo.trips().get(0).getDstid() + " \nhora: " + modelo.trips().get(0).getTimeParameter() + " \ntiempo promedio: " + modelo.trips().get(0).getGeometric_mean_travel_time() + "\n");
				    view.printMessage("----------------------------------------");
				    view.printMessage("Primer viaje:  \nzona origen: "+ modelo.trips().get(modelo.trips().size()-1).getSourceid()+"\nzona destino: "+ modelo.trips().get(modelo.trips().size()-1).getDstid() + " \nhora: " + modelo.trips().get(modelo.trips().size()-1).getTimeParameter() + " \ntiempo promedio: " + modelo.trips().get(modelo.trips().size()-1).getGeometric_mean_travel_time()+"\n");
					
					break;

				case 2:
					view.printMessage("--------- \nDar hora a revisar: ");
					dato = lector.next();
					int hour = Integer.parseInt(dato);
					if(hour<=24 && hour >=0)
					{
						view.printMessage("Viajes realizados a esa hora: " + modelo.tripsByHour(hour).size() + "\n---------");												
					}
					else
					{
						view.printMessage("Ingresar una hora valida.");	
					}
					break;

				case 3:					
					view.printMessage("--------- \nOrdenando con ShellSort ");
					long startTime = System.currentTimeMillis(); // medici�n tiempo actual
					Comparable<Trip>[] list =modelo.shell(modelo.tripsByHour(modelo.getActHour()));
					 long endTime = System.currentTimeMillis(); // medici�n tiempo actual
					 long duration = endTime - startTime; // duracion de ejecucion del algoritmo
					 view.printMessage("Tiempo de ordenamiento XXXXX: " + duration + " milisegundos");
					 view.printMessage("Primeros 10 viajes: \n ");
					 for(int i=0; i<10; i++)
					 {
						 Trip act = (Trip) list[i]; 
						view.printMessage("Viaje" + ""+ (i+1) + act.getTimeParameter() +"\n ");
					 }
					 view.printMessage("Ultimos 10 viajes: \n ");
					 for(int i=modelo.trips().size()-11; i<modelo.trips().size()-1; i++)
					 {
						 Trip act = modelo.trips().get(i); 
						 view.printMessage("Viaje" + ""+ (i+1) + act.getTimeParameter() +"\n ");
					 }
					break;

				case 4:
					System.out.println("--------- \nOrdenando con MergeSort ");
					long startTime2 = System.currentTimeMillis(); // medici�n tiempo actual
					Comparable<Trip>[] list2 = modelo.merge(modelo.tripsByHour(modelo.getActHour()));
					 long endTime2 = System.currentTimeMillis(); // medici�n tiempo actual
					 long duration2 = endTime2 - startTime2; // duracion de ejecucion del algoritmo
					 view.printMessage("Tiempo de ordenamiento Mergesort: " + duration2 + " milisegundos");
					 view.printMessage("Primeros 10 viajes: \n ");
					 for(int i=0; i<10; i++)
					 {
						Trip act = (Trip) list2[i]; 
						view.printMessage("Viaje" + ""+ (i+1) + act.getTimeParameter() +"\n ");
					 }
					 view.printMessage("Ultimos 10 viajes: \n ");
					 for(int i=modelo.trips().size()-11; i<modelo.trips().size()-1; i++)
					 {
						 Trip act = modelo.trips().get(i); 
						 view.printMessage("Viaje" + ""+ (i+1) + act.getTimeParameter() +"\n ");
					 }						
					break;

				case 5: 
					view.printMessage("--------- \nOrdenando con QuickSort ");
					long startTime3 = System.currentTimeMillis(); // medici�n tiempo actual
					Comparable<Trip>[] list3 =modelo.quick(modelo.tripsByHour(modelo.getActHour()));
					 long endTime3 = System.currentTimeMillis(); // medici�n tiempo actual
					 long duration3 = endTime3 - startTime3; // duracion de ejecucion del algoritmo
					 view.printMessage("Tiempo de ordenamiento XXXXX: " + duration3 + " milisegundos");
					 view.printMessage("Primeros 10 viajes: \n ");
					 for(int i=0; i<10; i++)
					 {
						 Trip act = (Trip) list3[i];  
						view.printMessage("Viaje" + ""+ (i+1) + act.getTimeParameter() +"\n ");
					 }
					 view.printMessage("Ultimos 10 viajes: \n ");
					 for(int i=modelo.trips().size()-11; i<modelo.trips().size()-1; i++)
					 {
						 Trip act = modelo.trips().get(i); 
						 view.printMessage("Viaje" + ""+ (i+1) + act.getTimeParameter() +"\n ");
					 }						
					break;	
					
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
